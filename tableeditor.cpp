/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>
#include <QtSql>

#include "tableeditor.h"

TableEditor::TableEditor(QSqlTableModel *(*getModel)(QObject *), QWidget *parent) : QDialog(parent) {
    this->model = (*getModel)(this);

    this->model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    this->model->select();

    this->view = new QTableView(this);
    view->setModel(this->model);
    view->resizeColumnsToContents();
    view->hideColumn(0); // TODO: FIX IT

    submitButton = new QPushButton(tr("&Save"));
    submitButton->setDefault(true);
    addButton = new QPushButton(tr("&Add"));
    deleteButton = new QPushButton(tr("&Delete"));
    revertButton = new QPushButton(tr("&Revert"));
    quitButton = new QPushButton(tr("&Close"));

    buttonBox = new QDialogButtonBox(Qt::Vertical);
    buttonBox->addButton(submitButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(addButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(deleteButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(revertButton, QDialogButtonBox::ActionRole);
    buttonBox->addButton(quitButton, QDialogButtonBox::RejectRole);

    connect(submitButton, &QPushButton::clicked, this, &TableEditor::submit);
    connect(addButton, &QPushButton::clicked, this, [this]() {
        this->model->insertRow(this->model->rowCount());
    }, Qt::QueuedConnection);
    connect(deleteButton, &QPushButton::clicked, this, [this]() {
        auto select = this->view->selectionModel();
        for (auto &index: select->selectedIndexes())
            this->model->removeRow(index.row());
    }, Qt::QueuedConnection);
    connect(revertButton, &QPushButton::clicked,  this->model, &QSqlTableModel::revertAll);
    connect(quitButton, &QPushButton::clicked, this, &TableEditor::close);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(view);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);

    setWindowTitle(this->model->tableName());
    this->setWindowState(Qt::WindowMaximized);
}

bool TableEditor::recordIsEmpty(const QSqlRecord &record) {
    for (int fieldIndex = 0; fieldIndex < record.count(); ++fieldIndex)
        if (!record.field(fieldIndex).isNull())
            return false;
    return true;
}

void TableEditor::removeEmptyRows() {
    for (int rowIndex = this->model->rowCount() - 1; 0 <= rowIndex; --rowIndex) {
        if (this->recordIsEmpty(this->model->record(rowIndex)))
            this->model->removeRow(rowIndex);
    }
}

void TableEditor::submit() {
    this->removeEmptyRows();
    model->database().transaction();
    if (model->submitAll()) {
        model->database().commit();
    } else {
        model->database().rollback();
        QMessageBox::warning(this, model->tableName(),
                             tr("The database reported an error: %1")
                             .arg(model->lastError().text()));
    }
}
