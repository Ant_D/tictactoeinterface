#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

#include <vector>

#include "const.h"
#include "player.h"
#include "position.h"

class Controller : public QObject {
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);
    FUNC_RESULT startNewGame(coord_T rowCount, coord_T columnCount, coord_T unitLength, Player *blackPlayer, Player *whitePlayer);
    FUNC_RESULT undo();
    FUNC_RESULT doMove(const Move &move);
    FUNC_RESULT clear();
    const Position *position() const;
    side_T currentPlayerSide() const;
    Player *currentPlayer() const;
    FUNC_RESULT saveGame(const QString &filePath);

private:
    Position _position;
    Player *blackPlayer = nullptr, *whitePlayer = nullptr;

    Player *nextPlayer() const;
    void connectPlayerSignals(Player *player);

signals:
    void moveIsDone(Move move);
    void moveIsIncorrect(Move move);
    void gameIsOver(side_T winner, std::vector<Move> winMoves);
    void playerError(QString title, QString content);

public slots:

private slots:
    void onMoveIsDone(Move move);
};

#endif // CONTROLLER_H
