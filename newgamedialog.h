#ifndef NEWGAMEDIALOG_H
#define NEWGAMEDIALOG_H

#include <QDialog>
#include <QToolButton>
#include <QComboBox>
#include <QRadioButton>

#include "const.h"

namespace Ui {
class NewGameDialog;
}

class NewGameDialog : public QDialog {
    Q_OBJECT

public:
    explicit NewGameDialog(QWidget *parent = 0);
    ~NewGameDialog();
    coord_T boardSize() const;
    coord_T unitLength() const;
    bool blackIsHuman() const;
    bool whiteIsHuman() const;
    bool blackIsComputer() const;
    bool whiteIsComputer() const;
    QString blackPlayerFilePath() const;
    QString whitePlayerFilePath() const;
    QString blackPlayerName() const;
    QString whitePlayerName() const;
    QString blackPlayerOptions() const;
    QString whitePlayerOptions() const;
    virtual void accept();

private:
    Ui::NewGameDialog *ui;
    QStringList engineFilePaths;
    QString blackEngineName, whiteEngineName;

    void setCurrentIfExists(QComboBox *comboBox, const QString &engineName);

private slots:
    void setMaxWinRowLen(int value);
    void setMinBoardSize(int value);
    void connectWidgets(QToolButton *toolBtn, QComboBox *comboBox, QRadioButton *humanRadioBtn, QRadioButton *computerRadioBtn);
    void updateEngineComboBox(QComboBox *comboBox, const QStringList &engines);

public slots:
    int exec();
};

#endif // NEWGAMEDIALOG_H
