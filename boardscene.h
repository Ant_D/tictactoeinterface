#ifndef BOARDSCENE_H
#define BOARDSCENE_H

#include <QGraphicsScene>
#include <QVector>

#include "cellitem.h"
#include "const.h"
#include "controller.h"
#include "mouseplayer.h"
#include "gtpplayer.h"

class BoardScene : public QGraphicsScene {
    Q_OBJECT
public:
    BoardScene(QObject *parent = nullptr);
    void startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength, Player *blackPlayer = nullptr, Player *whitePlayer = nullptr);
    void setSceneRect(const QRectF &rect);
    void setSceneRect(qreal x, qreal y, qreal w, qreal h);
    qreal cellSize() const;
    bool eventFilter(QObject *watched, QEvent *event);
    side_T currentPlayerSide() const;
    void undo();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    Controller *controller = nullptr;
    CellItem *cells[MAX_POSITION_SZ];
    coord_T rowsCount, columnsCount, unitLength;
    MousePlayer *mousePlayer = nullptr;
    CellItem *lastMoveCell = nullptr;
    bool isGameOver = false;
    QVector<Move> winMoves;

    void createCells();
    void updateCells();
    void destroyCells();
    coord_T cellIndex(coord_T rowIndex, coord_T columnIndex) const;
    CellItem *cell(coord_T rowIndex, coord_T columnIndex) const;
    void connectPlayerError(const QString &color, Player *player) const;

public slots:
    void onMoveIsDone(Move move);
    void saveGame(const QString &filePath) const;
};

#endif // BOARDSCENE_H
