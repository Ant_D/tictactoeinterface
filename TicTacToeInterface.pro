#-------------------------------------------------
#
# Project created by QtCreator 2018-06-17T13:48:39
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TicTacToeInterface
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    player.cpp \
    #gtpplayer.cpp \
    controller.cpp \
    mouseplayer.cpp \
    position.cpp \
    boardscene.cpp \
    boardview.cpp \
    cellitem.cpp \
    stoneitem.cpp \
    gtpplayer.cpp \
    newgamedialog.cpp \
    dbmanager.cpp \
    tableeditor.cpp

HEADERS += \
        mainwindow.h \
    player.h \
    #gtpplayer.h \
    controller.h \
    mouseplayer.h \
    position.h \
    boardscene.h \
    boardview.h \
    cellitem.h \
    const.h \
    funcResults.h \
    stoneitem.h \
    gtpplayer.h \
    newgamedialog.h \
    dbmanager.h \
    tableeditor.h

FORMS += \
        mainwindow.ui \
    newgamedialog.ui
