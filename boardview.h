#ifndef BOARDVIEW_H
#define BOARDVIEW_H

#include <QGraphicsView>

class BoardView : public QGraphicsView {
public:
    BoardView(QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    const qreal SCENE_MARGIN_FACTOR = 0.05;
};

#endif // BOARDVIEW_H
