#ifndef CELLITEM_H
#define CELLITEM_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

#include "const.h"
#include "stoneitem.h"

class CellItem : public QGraphicsRectItem {
public:
    CellItem(coord_T rowIndex, coord_T columnIndex, QGraphicsItem *parent = nullptr);
    coord_T rowIndex() const;
    coord_T columnIndex() const;
    void placeStone(side_T side, bool real = true);
    void removeStone();
    void placeMarker();
    void removeMarker();
    void updateRect(qreal x, qreal y, qreal w, qreal h);
    bool occupied() const;
    void setDefaultStyle();
    void setWinStyle();

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:
    const qreal STONE_SIZE_FACTOR = 0.8,
            MARKER_SIZE_FACTOR = 0.4;

    coord_T _rowIndex, _columnIndex;
    StoneItem *stoneItem;
    QGraphicsEllipseItem *markerItem;
    bool _occupied;

    qreal size() const;
    QRectF scaledRect(qreal factor);
};

#endif // CELLITEM_H
