#include "gtpplayer.h"

#include <QDebug>
#include <QRegExp>
#include <QStringBuilder>
#include <QVector>

static const QVector<CommandId> constCommands = {
    CommandId::NAME,
    CommandId::VERSION,
};


GtpPlayer::GtpPlayer(const QString &nameReceived, const QString &runCmd, QObject *parent)
    : Player(nameReceived, parent) {
    this->process = new QProcess(this);
    this->runCmd = runCmd;
    this->lastCmdNum = -1;
    this->genMoveMinNum = 0;
    this->movesDone = 0;

    connect(this->process, QProcess::errorOccurred, this, [this](QProcess::ProcessError error) {
        emit this->errorOccurred(((QProcess *)sender())->errorString());
    }, Qt::QueuedConnection);

    connect(this->process, SIGNAL(readyReadStandardOutput()), this, SLOT(onDataReception()), Qt::QueuedConnection);

    commandInfo[(int)CommandId::CLEAR_BOARD] = {"^\\d+ clear_board\\s*$", "", nullptr};
    commandInfo[(int)CommandId::GEN_MOVE] = {"^\\d+ genmove\\s*(w|b)\\s*$", "(\\w)(\\d+)\\s*$", &this->processGenMoveReply};
    commandInfo[(int)CommandId::NAME] = {"^\\d+ name\\s*$", "([\\w\\W]+)", nullptr};
    commandInfo[(int)CommandId::PLAY] = {"^\\d+ play\\s*(w|b)\\s*\\w\\d\\s*$", "", nullptr};
    commandInfo[(int)CommandId::QUIT] = {"^\\d+ quit\\s*$", "", nullptr};
    commandInfo[(int)CommandId::SET_BOARD_SIZE] = {"^\\d+ boardsize\\s*(\\d+)\\s*$", "", nullptr};
    commandInfo[(int)CommandId::SET_UNIT_LENGTH] = {"^\\d+ unitlength\\s*(\\d+)\\s*$", "", nullptr};
    commandInfo[(int)CommandId::UNDO] = {"^\\d+ undo\\s*$", "", nullptr};
    commandInfo[(int)CommandId::VERSION] = {"^\\d+ version\\s*$", "([\\w\\W]+)", nullptr};
}

GtpPlayer::GtpPlayer(const QString &name, const QString &filePath, const QString &options, QObject *parent) : GtpPlayer(name, filePath + " " + options, parent) {}

GtpPlayer::~GtpPlayer() {
    this->quit();
    this->process->waitForFinished(this->QUIT_TIME_DELAY);
}

void GtpPlayer::startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength) {
    this->Player::startNewGame(rowsCount, columnsCount, unitLength);
    if (this->process->state() == QProcess::NotRunning)
        this->process->start(this->runCmd);
    this->sendCmd(CommandId::SET_BOARD_SIZE, "boardsize %d", rowsCount);
    this->sendCmd(CommandId::SET_UNIT_LENGTH, "unitlength %d", unitLength);
    this->movesDone = 0;
}

void GtpPlayer::genMove(side_T side) {
    this->sendCmd(CommandId::GEN_MOVE, "genmove %c", SIDE_BLACK == side ? 'b' : 'w');
    ++this->movesDone;
}

void GtpPlayer::undoMove() {    
    this->sendCmd(CommandId::UNDO, "undo");
    --this->movesDone;
}

void GtpPlayer::doMove(const Move &move) {
    char column = 'a' + move.columnIndex + (move.columnIndex >= 8),
            color = SIDE_BLACK == move.color ? 'b' : 'w';
    coord_T row = this->rowsCount - move.rowIndex;

    this->sendCmd(CommandId::PLAY, "play %c %c%d", color, column, row);

    ++this->movesDone;
}

void GtpPlayer::clear() {
    this->sendCmd(CommandId::CLEAR_BOARD, "clear_board");
    this->movesDone = 0;
}

void GtpPlayer::goToMove(coord_T moveNum) {
    while (moveNum < this->movesDone) {
        this->undoMove();
    }
}

void GtpPlayer::sendCmd(CommandId cmdId, const char *fmt, ...) {
    size_t cmdSize = 0;
    char cmd[this->CMD_MAX_LEN];

    ++this->lastCmdNum;

    cmdSize += sprintf(cmd, "%d ", this->lastCmdNum);

    va_list args;
    va_start(args, fmt);
    cmdSize += vsprintf(cmd + cmdSize, fmt, args);
    va_end(args);
    cmdSize += sprintf(cmd + cmdSize, "\n");

    emit this->log(QString().sprintf("> %s", cmd));
    this->commands.push_back({cmdId, cmd});
    this->process->write(cmd);

    if (!constCommands.contains(cmdId))
        this->genMoveMinNum = this->lastCmdNum;
}

void GtpPlayer::quit() {
    this->sendCmd(CommandId::QUIT, "quit");
}

void GtpPlayer::processGenMoveReply(int cmdNum, const QRegExp &cmdPattern, const QRegExp &replyPattern) {
    char column;
    coord_T row, rowIndex, columnIndex;
    side_T side;

    // For example if undo command is present then genMove reply is not accepted
    if (cmdNum < this->genMoveMinNum)
        return;

    column = replyPattern.cap(1)[0].toLower().toLatin1();
    row = replyPattern.cap(2).toInt();
    rowIndex = this->rowsCount - row;
    columnIndex = column - 'a' - (column >= 'i');

    auto hmm = cmdPattern.cap(1);
    side = cmdPattern.cap(1) == "w" ? SIDE_WHITE : SIDE_BLACK;

    emit this->moveIsGenerated(Move(rowIndex, columnIndex, side));
}

bool GtpPlayer::tryReadLine(QString &reply) {
    this->line += this->process->readLine(this->CMD_MAX_LEN);
    if (this->CMD_MAX_LEN <= this->line.length()) {
        emit this->errorOccurred("Command buffer overflow");
        return false;
    }
    if (this->line[this->line.size() - 1] != '\n')
        return false;
    reply = QString(this->line.data());
    this->retrieveComment(reply);
    this->line.clear();
    return true;
}

void GtpPlayer::retrieveComment(QString &reply) {
    auto index = reply.indexOf('#');
    if (-1 != index)
        reply.truncate(index);
}

QString GtpPlayer::commandReplyError(const QString &cmd, const QString &reply) const {
    return "command: " % cmd.trimmed() % "\n" % "reply: " % reply.trimmed() % "\n";
}

void GtpPlayer::onDataReception() {
    const Command *cmd;
    const CommandInfo *cmdInfo;
    QRegExp cmdRegex, replyRegex;
    QString reply;
    int cmdNum;

    QRegExp failurePattern("^\\s*\\?\\s*");
    QRegExp successPattern("^\\s*=\\s*");
    QRegExp panicPattern("^\\s*!\\s*");
    QRegExp whitespacePattern("^\\s*$");
    QRegExp cmdNumPattern("^(\\d+)\\s*");

    while (0 < this->process->bytesAvailable()) {
        if (!this->tryReadLine(reply))
            continue;

        emit this->log(reply);
//        qDebug() << reply;

        if (-1 < whitespacePattern.indexIn(reply))
            continue;

        if (-1 < panicPattern.indexIn(reply)) {
            emit this->errorOccurred("PANIC REPLY\n" % reply);
            break;
        }

        if (this->commands.isEmpty()) {
            emit this->errorOccurred("Command stack is empty");
            break;
        }

        cmd = &this->commands.front();
        cmdInfo = this->commandInfo + int(cmd->id);
        cmdRegex.setPattern(cmdInfo->cmdPattern);
        replyRegex.setPattern(cmdInfo->replyPattern);

        if (-1 < successPattern.indexIn(reply)) {
            reply.remove(0, successPattern.matchedLength());
            if (-1 < cmdNumPattern.indexIn(reply)) {
                cmdNum = cmdNumPattern.cap().toInt();
                reply.remove(0, cmdNumPattern.matchedLength());
            }
            else {
                cmdNum = 0;
                emit this->errorOccurred("Command number is absent\n" % this->commandReplyError(cmd->cmd, reply));
            }
            cmdRegex.indexIn(cmd->cmd);
            if (-1 < replyRegex.indexIn(reply)) {
                if (nullptr != cmdInfo->processReply)
                    (this->*(cmdInfo->processReply))(cmdNum, cmdRegex, replyRegex);
            }
            else
                emit this->errorOccurred("PATTERN ERROR\n" % this->commandReplyError(cmd->cmd, reply));
        }
        else if (-1 < failurePattern.indexIn(reply))
            emit this->errorOccurred("FAIL REPLY\n" % this->commandReplyError(cmd->cmd, reply));
        else
            emit this->errorOccurred("PATTERN ERROR\n" % this->commandReplyError(cmd->cmd, reply));

        this->commands.pop_front();
    }
}
