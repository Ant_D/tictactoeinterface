#ifndef MOUSEPLAYER_H
#define MOUSEPLAYER_H

#include <QObject>

#include "player.h"

class MousePlayer : public Player {
    Q_OBJECT
public:
    MousePlayer(QObject *parent = nullptr);
    void genMove(side_T side);
    void undoMove();
    void doMove(const Move &move);
    void clear();
    void goToMove(coord_T moveNum);
};

#endif // MOUSEPLAYER_H
