#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>

#include "newgamedialog.h"
#include "boardscene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::MainWindow *ui = nullptr;
    NewGameDialog *newGameDialog = nullptr;
    BoardScene *boardScene = nullptr;
    Player *blackPlayer = nullptr,
            *whitePlayer = nullptr;

    void setPlayer(QTextEdit *textEdit, Player *&player, bool isComputer,
                   const QString &name, const QString &filePath, const QString &options);
};

#endif // MAINWINDOW_H
