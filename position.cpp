#include "position.h"

#include <algorithm>
#include <cstring>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

static const value_T INITIATIVE_VALUE = 1;
static const value_T VALUE_BASE = MAX(100, MAX_UNIT_LENGTH * 4 + INITIATIVE_VALUE + 1);

static bool valuePowInitialized = false;
static value_T valuePow[MAX_UNIT_LENGTH + 1];

static void valuePowInit() {
    valuePowInitialized = true;
    valuePow[0] = 1;
    for (coord_T i = 1; i <= MAX_UNIT_LENGTH; ++i)
        valuePow[i] = valuePow[i - 1] * VALUE_BASE;
}

side_T Unit::side() const {
    if (this->blackCount + this->whiteCount == 0)
        return SIDE_NONE;
    if (0 < this->blackCount && 0 < this->whiteCount)
        return SIDE_BOTH;
    if (0 < this->blackCount)
        return SIDE_BLACK;
    else
        return SIDE_WHITE;
}

value_T Unit::value() const {
    switch (this->side()) {
        case SIDE_NONE: return 1;
        case SIDE_BLACK: return valuePow[this->blackCount];
        case SIDE_WHITE: return -valuePow[this->whiteCount];
        default: return 0;
    }
}

inline bool Position::colorIsCorrect(side_T color) {
    return color == SIDE_NONE || color == SIDE_BLACK || color == SIDE_WHITE;
}

inline bool Position::inside(coord_T rowIndex, coord_T columnIndex) const {
    return 0 <= rowIndex && rowIndex < this->_rowsCount &&
               0 <= columnIndex && columnIndex < this->_columnsCount;
}

inline bool Position::moveIsCorrect(const Move &move) const {
    return move.isPass || (this->inside(move.rowIndex, move.columnIndex) &&
                (this->_moveHistorySize % 2 == 0 ? move.color == SIDE_BLACK : move.color == SIDE_WHITE) &&
                this->get(move.rowIndex, move.columnIndex) == SIDE_NONE);
}

inline coord_T Position::cellIndex(coord_T rowIndex, coord_T columnIndex) const {
    return this->_columnsCount * rowIndex + columnIndex;
}

inline Move Position::cellIndexToMove(coord_T index) const {
    return Move(index / this->_columnsCount, index % this->_columnsCount, this->position[index]);
}

void Position::unitToMoves(const Unit &unit, std::vector<Move> &moves) const {
    moves.resize(0);
    for (coord_T index = 0; index < this->_unitLength; ++index)
        moves.push_back(this->cellIndexToMove(unit.cells[index]));
}

FUNC_RESULT Position::addMoveToMoveHistory(const Move &move) {
    Move *nextMove;

    if (this->_moveHistorySize >= MAX_MOVE_HISTORY_SZ)
        return FUNC_RESULT_FAILED;

    nextMove = this->_moveHistory + this->_moveHistorySize;
    *nextMove = move;
    ++this->_moveHistorySize;

    return FUNC_RESULT_SUCCESS;
}

void Position::initUnits() {
    const coord_T X_DIR[] = {1, 1, 0, -1},
                Y_DIR[] = {0, 1, 1, 1};
    coord_T cell;
    Unit *unit;

    this->unitsCount = 0;
    memset(this->cellToUnitsCount, 0, sizeof(this->cellToUnitsCount));

    for (coord_T rowIndex = 0; rowIndex < this->_rowsCount; ++rowIndex)
        for (coord_T columnIndex = 0; columnIndex < this->_columnsCount; ++columnIndex)
            for (int dir = 0; dir < 4; ++dir)
                if (this->inside((coord_T)(rowIndex + (this->_unitLength - 1) * Y_DIR[dir]),
                                 (coord_T)(columnIndex + (this->_unitLength - 1) * X_DIR[dir]))) {
                    unit = this->units + this->unitsCount;
                    ++this->unitsCount;
                    for (coord_T i = 0; i < this->_unitLength; ++i) {
                        cell = this->cellIndex(rowIndex + i * Y_DIR[dir], columnIndex + i * X_DIR[dir]);
                        unit->cells[i] = cell;
                        this->cellToUnits[cell][this->cellToUnitsCount[cell]++] = unit;
                    }
                }
}

void Position::updateUnits(coord_T rowIndex, coord_T columnIndex, side_T oldColor, side_T newColor) {
    Unit *unit;
    coord_T cell = this->cellIndex(rowIndex, columnIndex);

    for (coord_T index = 0; index < this->cellToUnitsCount[cell]; ++index) {
        unit = this->cellToUnits[cell][index];
        if (SIDE_BLACK == oldColor)
            --unit->blackCount;
        else if (SIDE_WHITE == oldColor)
            --unit->whiteCount;
        if (SIDE_BLACK == newColor)
            ++unit->blackCount;
        else if (SIDE_WHITE == newColor)
            ++unit->whiteCount;
    }
}

Position::Position() {
    this->initialized = false;
}

Position *Position::create(coord_T rowsCount, coord_T columnsCount, coord_T unitLength)
{
    Position *position;
    FUNC_RESULT result;

    position = new Position;
    if (nullptr == position)
        return position;

    result = position->init(rowsCount, columnsCount, unitLength);
    if (FUNC_RESULT_SUCCESS != result) {
        delete position;
        return nullptr;
    }

    return position;
}

FUNC_RESULT Position::init(coord_T rowsCount, coord_T columnsCount, coord_T unitLength) {
    if (!(MIN_POSITION_WIDTH <= rowsCount && rowsCount <= MAX_POSITION_WIDTH) ||
        !(MIN_POSITION_WIDTH <= columnsCount && columnsCount <= MAX_POSITION_WIDTH) ||
        !(MIN_UNIT_LENGTH <= unitLength && unitLength <= MIN(MAX_UNIT_LENGTH, MAX(rowsCount, columnsCount))))
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (!valuePowInitialized) {
        valuePowInitialized = true;
        valuePowInit();
    }

    this->_rowsCount = rowsCount;
    this->_columnsCount = columnsCount;
    this->size = rowsCount * columnsCount;
    this->_unitLength = unitLength;

    this->initialized = true;
    this->initUnits();
    this->clear();

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT Position::clear() {
    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    for (Unit *unit = this->units; unit - this->units < this->unitsCount; ++unit) {
        unit->whiteCount = unit->blackCount = 0;
    }

    this->_moveHistorySize = 0;
    for (coord_T index = 0; index < this->size; ++index)
        this->position[index] = SIDE_NONE;

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT Position::placeColor(coord_T rowIndex, coord_T columnIndex, side_T color) {
    coord_T index;
    side_T oldColor;

    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    if (!this->inside(rowIndex, columnIndex) || !this->colorIsCorrect(color))
        return FUNC_RESULT_FAILED_ARGUMENT;

    index = this->cellIndex(rowIndex, columnIndex);
    oldColor = this->position[index];

    this->updateUnits(rowIndex, columnIndex, oldColor, color);

    this->position[index] = color;

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT Position::undo() {
    const Move *move;

    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    if (0 == this->_moveHistorySize)
        return FUNC_RESULT_FAILED;

    move = this->_moveHistory + this->_moveHistorySize - 1;
    --this->_moveHistorySize;

    return this->placeColor(move->rowIndex, move->columnIndex, SIDE_NONE);
}

FUNC_RESULT Position::doMove(const Move &move) {
    FUNC_RESULT result;

    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    if (!this->moveIsCorrect(move))
        return FUNC_RESULT_FAILED_ARGUMENT;

    if (!move.isPass) {
        result = this->placeColor(move.rowIndex, move.columnIndex, move.color);
        if (FUNC_RESULT_SUCCESS != result)
            return result;
    }

    return this->addMoveToMoveHistory(move);
}

inline side_T Position::get(coord_T rowIndex, coord_T columnIndex) const {
    if (!this->inside(rowIndex, columnIndex))
        return SIDE_NONE;
    return this->position[this->cellIndex(rowIndex, columnIndex)];
}

FUNC_RESULT Position::print(FILE *out) const {
    const int WIDTH = 3, NUM_WIDTH = 3;
    side_T side;
    const char *symbol, *alphabet = "ABCDEFGHJKLMNOPQRSTUVWXYZ";

    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    if (nullptr == out)
        return FUNC_RESULT_FAILED_ARGUMENT;

    fprintf(out, "\n%*s", NUM_WIDTH, "");
    for (coord_T columnIndex = 0; columnIndex < this->_columnsCount; ++columnIndex)
        fprintf(out, " %*c%*s", WIDTH / 2 + WIDTH % 2, alphabet[columnIndex], WIDTH / 2, "");
    fprintf(out, "\n");

    for (coord_T rowIndex = 0; rowIndex < this->_rowsCount + 1; ++rowIndex) {
        fprintf(out, "%*s", NUM_WIDTH, "");
        for (coord_T j = 0; j < (WIDTH + 1) * this->_columnsCount + 1; ++j)
            fprintf(out, "-");
        if (rowIndex == this->_rowsCount)
            break;
        fprintf(out, "\n%-*d", NUM_WIDTH, this->_rowsCount - rowIndex);
        for (coord_T columnIndex = 0; columnIndex < this->_columnsCount; ++columnIndex) {
            side = this->get(rowIndex, columnIndex);
            if (side == SIDE_BLACK)
                symbol = "X";
            else if (side == SIDE_WHITE)
                symbol = "O";
            else
                symbol = " ";
            fprintf(out, "|%*s%*s", WIDTH / 2 + WIDTH % 2, symbol, WIDTH / 2, "");
        }
        fprintf(out, "|%*d\n", NUM_WIDTH, this->_rowsCount - rowIndex);
    }

    fprintf(out, "\n%*s", NUM_WIDTH, "");
    for (coord_T columnIndex = 0; columnIndex < this->_columnsCount; ++columnIndex)
        fprintf(out, " %*c%*s", WIDTH / 2 + WIDTH % 2, alphabet[columnIndex], WIDTH / 2, "");
    fprintf(out, "\n");

    return FUNC_RESULT_SUCCESS;
}

bool Position::isGameOver(side_T &winner, std::vector<Move> &winMoves) const {
    const Unit *unit;
    side_T _winner;

    winMoves.resize(0);

    if (!this->initialized || this->_moveHistorySize == this->size) {
        winner = SIDE_BOTH;
        return true;
    }

    _winner = SIDE_NONE;
    for (coord_T index = 0; index < this->unitsCount; ++index) {
        unit = this->units + index;
        if (unit->blackCount == this->_unitLength) {
            _winner = SIDE_BLACK;
            this->unitToMoves(*unit, winMoves);
            break;
        }
        else if (unit->whiteCount == this->_unitLength) {
            _winner = SIDE_WHITE;
            this->unitToMoves(*unit, winMoves);
            break;
        }
    }

    winner = _winner;
    return SIDE_NONE != _winner;
}

bool Position::moveIsWinning(coord_T rowIndex, coord_T columnIndex, side_T color) const {
    coord_T cell, colorCount;
    const Unit *unit;

    if (!this->initialized || !this->inside(rowIndex, columnIndex) ||
        (SIDE_BLACK != color && SIDE_WHITE != color))
        return false;

    cell = this->cellIndex(rowIndex, columnIndex);

    for (coord_T index = 0; index < this->cellToUnitsCount[cell]; ++index) {
        unit = this->cellToUnits[cell][index];
        colorCount = SIDE_BLACK == color ? unit->blackCount : unit->whiteCount;
        if (unit->side() == color && colorCount == this->_unitLength - 1)
            return true;
    }

    return false;
}

FUNC_RESULT Position::getCellValue(coord_T rowIndex, coord_T columnIndex, side_T side, value_T &value) const {
    coord_T cell;
    Unit *unit;

    if (!this->initialized)
        return FUNC_RESULT_FAILED;

    if (!this->inside(rowIndex, columnIndex) ||
        (SIDE_BLACK != side && SIDE_WHITE != side && SIDE_BOTH != side))
        return FUNC_RESULT_FAILED_ARGUMENT;

    cell = this->cellIndex(rowIndex, columnIndex);
    value = 0;

    for (coord_T index = 0; index < this->cellToUnitsCount[cell]; ++index) {
        unit = this->cellToUnits[cell][index];
        if (SIDE_BOTH == side || unit->side() == side || unit->side() == SIDE_NONE)
            value += std::abs(unit->value());
    }

    return FUNC_RESULT_SUCCESS;
}

side_T Position::currentPlayer() const {
    return this->_moveHistorySize % 2 == 0 ? SIDE_BLACK : SIDE_WHITE;
}

const Move *Position::lastMove() const {
    return 0 < this->_moveHistorySize ? this->_moveHistory + this->_moveHistorySize - 1 : nullptr;
}

coord_T Position::rowsCount() const {
    return this->_rowsCount;
}

coord_T Position::columnsCount() const {
    return this->_columnsCount;
}

coord_T Position::moveHistorySize() const {
    return this->_moveHistorySize;
}

const Move *Position::moveHistory() const {
    return this->_moveHistory;
}

Move::Move() {
    
}

Move::Move(coord_T rowIndex, coord_T columnIndex, side_T color, bool isPass)
    : rowIndex(rowIndex), columnIndex(columnIndex), color(color), isPass(isPass) {

}
