#include "boardview.h"

#include "boardscene.h"

BoardView::BoardView(QWidget *parent) : QGraphicsView(parent) {
}

void BoardView::resizeEvent(QResizeEvent *event) {
    this->QGraphicsView::resizeEvent(event);

    BoardScene *scene = dynamic_cast<BoardScene *>(this->scene());
    if (nullptr != scene)
        scene->setSceneRect(0, 0,
                            this->width() * (1 - this->SCENE_MARGIN_FACTOR),
                            this->height() * (1 - this->SCENE_MARGIN_FACTOR));
}
