#ifndef POSITION_H
#define POSITION_H

#include <cstdio>
#include <cstdint>
#include <vector>

#include "const.h"
#include "funcResults.h"

typedef int64_t value_T;

struct Move {
    Move();
    Move(coord_T rowIndex, coord_T columnIndex, side_T color, bool isPass = false);

    coord_T rowIndex;
    coord_T columnIndex;
    side_T color;
    bool isPass;
};

struct Unit {
    coord_T blackCount;
    coord_T whiteCount;
    coord_T cells[MAX_UNIT_LENGTH];

    side_T side() const;
    value_T value() const;
};

class Position {
public:
    Position();
    static Position *create(coord_T _rowsCount, coord_T _columnsCount, coord_T _unitLength);
    FUNC_RESULT init(coord_T _rowsCount, coord_T _columnsCount, coord_T _unitLength);
    FUNC_RESULT clear();
    FUNC_RESULT placeColor(coord_T rowIndex, coord_T columnIndex, side_T color);
    FUNC_RESULT doMove(const Move &move);
    FUNC_RESULT undo();
    side_T get(coord_T rowIndex, coord_T columnIndex) const;
    bool isGameOver(side_T &winner, std::vector<Move> &winMoves) const;
    bool moveIsWinning(coord_T rowIndex, coord_T columnIndex, side_T color) const;
    FUNC_RESULT print(FILE *out) const;
    FUNC_RESULT getCellValue(coord_T rowIndex, coord_T columnIndex, side_T side, value_T &value) const;
    side_T currentPlayer() const;
    const Move *lastMove() const;
    coord_T rowsCount() const;
    coord_T columnsCount() const;
    coord_T moveHistorySize() const;
    const Move *moveHistory() const;

private:
    bool initialized;
    coord_T _rowsCount;
    coord_T _columnsCount;
    coord_T _unitLength;
    coord_T size;
    coord_T _moveHistorySize;
    side_T position[MAX_POSITION_SZ];
    Move _moveHistory[MAX_MOVE_HISTORY_SZ];
    coord_T unitsCount;
    Unit units[MAX_UNIT_COUNT];
    coord_T cellToUnitsCount[MAX_POSITION_SZ];
    Unit *cellToUnits[MAX_POSITION_SZ][MAX_UNIT_COUNT_PER_CELL];

    inline bool colorIsCorrect(side_T color);
    inline bool inside(coord_T rowIndex, coord_T columnIndex) const;
    inline bool moveIsCorrect(const Move &move) const;
    inline coord_T cellIndex(coord_T rowIndex, coord_T columnIndex) const;
    FUNC_RESULT addMoveToMoveHistory(const Move &move);
    void initUnits();
    void updateUnits(coord_T rowIndex, coord_T columnIndex, side_T oldColor, side_T newColor);
    Move cellIndexToMove(coord_T cellIndex) const;
    void unitToMoves(const Unit &unit, std::vector<Move> &moves) const;
};

#endif // POSITION_H
