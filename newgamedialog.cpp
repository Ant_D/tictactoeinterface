#include "newgamedialog.h"
#include "ui_newgamedialog.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include <algorithm>

#include "dbmanager.h"

NewGameDialog::NewGameDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewGameDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("New game dialog");

    connect(this->ui->boardSizeSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(setMaxWinRowLen(int)), Qt::QueuedConnection);

    connect(this->ui->winRowLenSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(setMinBoardSize(int)), Qt::QueuedConnection);

    this->ui->boardSizeSpinBox->setValue(MAX_POSITION_WIDTH);
    this->ui->boardSizeSpinBox->setMaximum(MAX_POSITION_WIDTH);

    this->ui->winRowLenSpinBox->setValue(MAX_UNIT_LENGTH);
    this->ui->winRowLenSpinBox->setMinimum(MIN_UNIT_LENGTH);

    this->connectWidgets(this->ui->blackToolButton, this->ui->blackEngineComboBox,
                         this->ui->blackHumanRadioButton, this->ui->blackComputerRadioButton);
    this->connectWidgets(this->ui->whiteToolButton, this->ui->whiteEngineComboBox,
                         this->ui->whiteHumanRadioButton, this->ui->whiteComputerRadioButton);
}

NewGameDialog::~NewGameDialog() {
    delete ui;
}

coord_T NewGameDialog::boardSize() const {
    return this->ui->boardSizeSpinBox->value();
}

coord_T NewGameDialog::unitLength() const {
    return this->ui->winRowLenSpinBox->value();
}

bool NewGameDialog::blackIsHuman() const {
    return this->ui->blackHumanRadioButton->isChecked();
}

bool NewGameDialog::whiteIsHuman() const {
    return this->ui->whiteHumanRadioButton->isChecked();
}

bool NewGameDialog::blackIsComputer() const {
    return this->ui->blackComputerRadioButton->isChecked();
}

bool NewGameDialog::whiteIsComputer() const {
    return this->ui->whiteComputerRadioButton->isChecked();
}

QString NewGameDialog::blackPlayerFilePath() const {
    auto index = this->ui->blackEngineComboBox->currentIndex();
    return 0 <= index && index < this->engineFilePaths.length() ? this->engineFilePaths.at(index) : "";
}

QString NewGameDialog::whitePlayerFilePath() const {
    auto index = this->ui->whiteEngineComboBox->currentIndex();
    return 0 <= index && index < this->engineFilePaths.length() ? this->engineFilePaths.at(index) : "";
}

QString NewGameDialog::blackPlayerName() const {
    if (blackIsComputer())
        return this->ui->blackEngineComboBox->currentText();
    return "";
}

QString NewGameDialog::whitePlayerName() const {
    if (whiteIsComputer())
        return this->ui->whiteEngineComboBox->currentText();
    return "";
}

QString NewGameDialog::blackPlayerOptions() const {
    auto index = this->ui->blackEngineComboBox->currentIndex();
    if (blackIsComputer() && 0 <= index && index < this->engineFilePaths.length()) {
        return DbManager::getEngineOptionsByName(this->blackPlayerName());
    }
    return "";
}

QString NewGameDialog::whitePlayerOptions() const {
    auto index = this->ui->whiteEngineComboBox->currentIndex();
    if (whiteIsComputer() && 0 <= index && index < this->engineFilePaths.length()) {
        return DbManager::getEngineOptionsByName(this->whitePlayerName());
    }
    return "";
}

void NewGameDialog::accept() {
    if ((this->blackIsComputer() && this->ui->blackEngineComboBox->currentIndex() == -1) ||
        (this->whiteIsComputer() && this->ui->whiteEngineComboBox->currentIndex() == -1))
        QMessageBox(QMessageBox::Warning, "", "Provide file path for computer player").exec();
    else {
        if (this->blackIsComputer())
            this->blackEngineName = this->ui->blackEngineComboBox->currentText();
        if (this->whiteIsComputer())
            this->whiteEngineName = this->ui->whiteEngineComboBox->currentText();
        QDialog::accept();
    }
}

void NewGameDialog::setCurrentIfExists(QComboBox *comboBox, const QString &engineName) {
    auto index = comboBox->findText(engineName);
    if (-1 != index)
        comboBox->setCurrentIndex(index);
}

void NewGameDialog::setMinBoardSize(int value) {
    this->ui->boardSizeSpinBox->setMinimum(std::max(MIN_POSITION_WIDTH, value));
}

void NewGameDialog::setMaxWinRowLen(int value) {
    this->ui->winRowLenSpinBox->setMaximum(std::min(MAX_UNIT_LENGTH, value));
}

void NewGameDialog::connectWidgets(QToolButton *toolBtn, QComboBox *comboBox, QRadioButton *humanRadioBtn, QRadioButton *computerRadioBtn) {
    connect(computerRadioBtn, QRadioButton::toggled, this, [=](bool checked) {
        comboBox->setEnabled(checked);
        comboBox->setEditable(checked);
        toolBtn->setEnabled(checked);
    }, Qt::QueuedConnection);

//    connect(toolBtn, QToolButton::pressed, this, [=]() {
//        auto fileDialog = new QFileDialog(this);
//        connect(fileDialog, QFileDialog::fileSelected, this, [=](QString file) {
//            comboBox->addItem(file);
//            comboBox->setCurrentIndex(comboBox->count() - 1);
//        }, Qt::QueuedConnection);
//        connect(fileDialog, QFileDialog::finished, this, [fileDialog]() {
//            delete fileDialog;
//        }, Qt::QueuedConnection);
//        fileDialog->exec();
//    }, Qt::QueuedConnection);
}

void NewGameDialog::updateEngineComboBox(QComboBox *comboBox, const QStringList &engines) {
    comboBox->clear();
    for (const auto &engine: engines)
        comboBox->addItem(engine);
}

int NewGameDialog::exec() {
    QStringList engineNames = DbManager::getEngineNameList();

    this->engineFilePaths = DbManager::getEngineFilePathList(); // TODO: unite get name list and get path list

    this->updateEngineComboBox(this->ui->blackEngineComboBox, engineNames);
    this->updateEngineComboBox(this->ui->whiteEngineComboBox, engineNames);

    this->setCurrentIfExists(this->ui->blackEngineComboBox, this->blackEngineName);
    this->setCurrentIfExists(this->ui->whiteEngineComboBox, this->whiteEngineName);

    return QDialog::exec();
}
