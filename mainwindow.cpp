#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QKeyEvent>

#include "boardscene.h"
#include "dbmanager.h"
#include "gtpplayer.h"
#include "mouseplayer.h"
#include "newgamedialog.h"
#include "tableeditor.h"

template<typename T>
void safeDelete(T *&ptr) {
    delete ptr;
    ptr = nullptr;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->ui->setupUi(this);

    this->newGameDialog = new NewGameDialog(this);

    this->boardScene = new BoardScene(this->ui->boardView);
    this->ui->boardView->setScene(this->boardScene);

    connect(this->ui->actionNewGame, QAction::triggered, this->newGameDialog, NewGameDialog::exec, Qt::QueuedConnection);

    connect(this->ui->actionManageEngines, QAction::triggered, this, []() {
        TableEditor tableEditor(&DbManager::getEnginesModel);
        tableEditor.exec();
    }, Qt::QueuedConnection);

    connect(this->ui->actionLogs, QAction::triggered, this, [this]() {
        this->ui->blackTextEdit->setVisible(!this->ui->blackTextEdit->isVisible());
        this->ui->whiteTextEdit->setVisible(!this->ui->whiteTextEdit->isVisible());
    }, Qt::QueuedConnection);

    connect(this->ui->actionSave, QAction::triggered, this, [this]() {
        auto *dialog = new QFileDialog;
        dialog->setAcceptMode(QFileDialog::AcceptSave);
        dialog->setDefaultSuffix(".sgf");
        connect(dialog, QFileDialog::fileSelected, this->boardScene, BoardScene::saveGame, Qt::QueuedConnection);
        connect(dialog, QFileDialog::finished, this, [dialog]() {
            delete dialog;
        }, Qt::QueuedConnection);
        dialog->exec();
    }, Qt::QueuedConnection);

    connect(newGameDialog, NewGameDialog::accepted, this, [this]() {
        this->setPlayer(this->ui->blackTextEdit, this->blackPlayer,
                        this->newGameDialog->blackIsComputer(),
                        this->newGameDialog->blackPlayerName(),
                        this->newGameDialog->blackPlayerFilePath(),
                        this->newGameDialog->blackPlayerOptions());
        this->setPlayer(this->ui->whiteTextEdit, this->whitePlayer,
                        this->newGameDialog->whiteIsComputer(),
                        this->newGameDialog->whitePlayerName(),
                        this->newGameDialog->whitePlayerFilePath(),
                        this->newGameDialog->whitePlayerOptions());
        this->boardScene->startNewGame(this->newGameDialog->boardSize(), this->newGameDialog->boardSize(),
                                       this->newGameDialog->unitLength(), this->blackPlayer, this->whitePlayer);
    }, Qt::QueuedConnection);

    this->newGameDialog->accept();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (Qt::ControlModifier & event->modifiers()) {
        if (Qt::Key_N == event->key())
            this->ui->actionNewGame->trigger();
        else if (Qt::Key_M == event->key())
            this->ui->actionManageEngines->trigger();
        else if (Qt::Key_L == event->key())
            this->ui->actionLogs->trigger();
        else if (Qt::Key_U == event->key())
            this->boardScene->undo();
    }
}

void MainWindow::setPlayer(QTextEdit *textEdit, Player *&player, bool isComputer,
                           const QString &name, const QString &filePath, const QString &options) {
    if (nullptr != player)
        safeDelete(player);
    textEdit->clear();
    if (isComputer) {
        player = new GtpPlayer(name, filePath, options, this);
        connect(player, Player::log, textEdit, QTextEdit::append);
    }
}
