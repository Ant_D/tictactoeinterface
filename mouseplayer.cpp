#include "mouseplayer.h"

MousePlayer::MousePlayer(QObject *parent) : Player("Human", parent) {}

void MousePlayer::genMove(side_T side) {}

void MousePlayer::undoMove() {}

void MousePlayer::doMove(const Move &move) {}

void MousePlayer::clear() {}

void MousePlayer::goToMove(coord_T moveNum) {}
