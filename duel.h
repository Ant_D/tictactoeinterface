#ifndef DUEL_H
#define DUEL_H

#include <QObject>

#include "player.h"
#include "controller.h"

class Duel : public QObject {
    Q_OBJECT
public:
    explicit Duel(coord_T rowsCount, coord_T columnsCount, coord_T unitLength,
                  Player *blackPlayer, Player *whitePlayer, int gamesCount, QObject *parent = nullptr);

public slots:
    void printResult();
    void run();
    void finish();

signals:
    void finished();

private:
    coord_T rowsCount;
    coord_T columnsCount;
    coord_T unitLength;
    Player *blackPlayer;
    Player *whitePlayer;
    int gamesCount;
    int scores[4];
    Controller *controller;

private slots:
    void handleGameOver(side_T winner);
};

#endif // DUEL_H
