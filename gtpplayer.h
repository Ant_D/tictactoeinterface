#ifndef GTPPLAYER_H
#define GTPPLAYER_H

#include <QList>
#include <QObject>
#include <QProcess>

#include "player.h"

enum class CommandId {
    SET_BOARD_SIZE,
    SET_UNIT_LENGTH,
    CLEAR_BOARD,
    GEN_MOVE,
    PLAY,
    UNDO,
    QUIT,
    NAME,
    VERSION,
    COUNT
};

class GtpPlayer;

struct CommandInfo {
    QString cmdPattern;
    QString replyPattern;
    void (GtpPlayer::*processReply)(int cmdNum, const QRegExp &cmdRegex, const QRegExp &replyRegex);
};

struct Command {
    CommandId id;
    QString cmd;
};

class GtpPlayer : public Player {
    Q_OBJECT
public:
    GtpPlayer(const QString &nameReceived, const QString &cmd, QObject *parent = nullptr);
    GtpPlayer(const QString &nameReceived, const QString &filePath, const QString &options, QObject *parent = nullptr);
    ~GtpPlayer();
    void startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength);
    void genMove(side_T side);
    void undoMove();
    void doMove(const Move &move);
    void clear();
    void goToMove(coord_T moveNum);

private:
    const int CMD_MAX_LEN = 1024;
    const size_t QUIT_TIME_DELAY = 1000;

    QProcess *process;
    QList<Command> commands;
    QByteArray line;
    CommandInfo commandInfo[(int)CommandId::COUNT];
    int lastCmdNum;
    int genMoveMinNum;  // genMove reply will be accepted if it's number >= this
    QString runCmd;
    int movesDone;

    void sendCmd(CommandId cmdId, const char *fmt, ...);
    void quit();
    void processGenMoveReply(int cmdNum, const QRegExp &cmdPattern, const QRegExp &replyPattern);
    bool tryReadLine(QString &reply);
    void retrieveComment(QString &reply);
    QString commandReplyError(const QString &cmd, const QString &reply) const;

private slots:
    void onDataReception();
};


#endif // GTPPLAYER_H
