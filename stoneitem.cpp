#include "stoneitem.h"

#include <QBrush>
#include <QPen>

#include "const.h"

StoneItem::StoneItem(const QRectF &rect, side_T side, bool pale, QGraphicsItem *parent) : QGraphicsEllipseItem(parent) {
    this->setRect(rect);
    this->_side = side;

    this->setBrush(QBrush(SIDE_BLACK == side ? Qt::black : Qt::white));

    qreal borderWidth = std::min(rect.width(), rect.height()) * this->BORDER_WIDTH_FACTOR;
    this->setPen(QPen(QBrush(Qt::black), borderWidth));

    if (pale)
        this->setOpacity(0.5);
}

side_T StoneItem::side() const {
    return this->_side;
}
