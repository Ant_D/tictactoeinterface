#include "duel.h"

#include <iostream>

Duel::Duel(coord_T rowsCount, coord_T columnsCount, coord_T unitLength,
           Player *blackPlayer, Player *whitePlayer, int gamesCount, QObject *parent)
    : QObject(parent), rowsCount(rowsCount), columnsCount(columnsCount), unitLength(unitLength),
      blackPlayer(blackPlayer), whitePlayer(whitePlayer), gamesCount(gamesCount) {
    this->controller = new Controller(this);
    for (int i = 0; i < 4; ++i)
        this->scores[i] = 0;
    connect(this->controller, SIGNAL(gameIsOver(side_T, std::vector<Move>)), this, SLOT(handleGameOver(side_T)),
            Qt::QueuedConnection);
}

void Duel::printResult() {
    std::cout << "b " << this->scores[SIDE_BLACK]
                << ", w " << this->scores[SIDE_WHITE]
                << ", d " << this->scores[SIDE_BOTH] << "\n";
}

void Duel::run() {
    this->controller->startNewGame(this->rowsCount, this->columnsCount, this->unitLength,
                                   this->blackPlayer, this->whitePlayer);
}

void Duel::finish() {
    emit this->finished();
}

void Duel::handleGameOver(side_T winner) {
    ++scores[winner];
    --this->gamesCount;
    if (this->gamesCount <= 0) {
        this->printResult();
        this->finish();
    }
    else
        this->run();
}
