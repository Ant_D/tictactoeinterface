#include <QCoreApplication>
#include <QCommandLineParser>
#include <QObject>
#include <QTimer>
#include <QThread>

#include <iostream>
#include <vector>

#include "controller.h"
#include "duel.h"
#include "gtpplayer.h"

Q_DECLARE_METATYPE(Move);
Q_DECLARE_METATYPE(side_T);
Q_DECLARE_METATYPE(std::vector<Move>);

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QCommandLineParser parser;

    parser.addHelpOption();

    parser.addOptions({
        {QStringList() << "f" << "first",
        QCoreApplication::translate("main", "First player"),
        "Path"},
        {QStringList() << "s" << "second",
        QCoreApplication::translate("main", "Second player"),
        "Path"},
        {QStringList() << "b" << "board",
        QCoreApplication::translate("main", "Board width, height, unit length separated by semicolon"),
        "Group of three positive integers"},
        {QStringList() << "g" << "games",
        QCoreApplication::translate("main", "Games count"),
        "Positive integer"},
    });

    parser.process(app);
    QStringList boardParams = parser.value("board").split(";");
    coord_T rowsCount = static_cast<coord_T>(boardParams[0].toInt()),
            columnsCount = static_cast<coord_T>(boardParams[1].toInt()),
            unitLength = static_cast<coord_T>(boardParams[2].toInt());
    GtpPlayer *blackPlayer = new GtpPlayer("black", parser.value("first"), &app);
    GtpPlayer *whitePlayer = new GtpPlayer("white", parser.value("second"), &app);
    int gamesCount = parser.value("games").toInt();

    qRegisterMetaType<Move>();
    qRegisterMetaType<side_T>();
    qRegisterMetaType<std::vector<Move>>();

    Duel *duel = new Duel(rowsCount, columnsCount, unitLength, blackPlayer, whitePlayer, gamesCount, &app);

    QObject::connect(duel, SIGNAL(finished()), &app, SLOT(quit()), Qt::QueuedConnection);

    QTimer::singleShot(0, duel, SLOT(run()));

    return app.exec();
}
