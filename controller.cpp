#include "controller.h"

#include <QDebug>
#include <QFile>

#include <algorithm>

#include "mouseplayer.h"

Controller::Controller(QObject *parent) : QObject(parent) {}

FUNC_RESULT Controller::startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength, Player *blackPlayer, Player *whitePlayer) {
    FUNC_RESULT result;

    if (nullptr == blackPlayer || nullptr == whitePlayer)
        return FUNC_RESULT_FAILED_ARGUMENT;

    result = this->_position.init(rowsCount, columnsCount, unitLength);
    if (FUNC_RESULT_SUCCESS != result)
        return result;

    this->blackPlayer = blackPlayer;
    this->whitePlayer = whitePlayer;

    this->blackPlayer->startNewGame(rowsCount, columnsCount, unitLength);
    this->whitePlayer->startNewGame(rowsCount, columnsCount, unitLength);

    this->connectPlayerSignals(blackPlayer);
    this->connectPlayerSignals(whitePlayer);

    blackPlayer->genMove(SIDE_BLACK);

    return FUNC_RESULT_SUCCESS;
}

void Controller::connectPlayerSignals(Player *player) {
    connect(player, Player::moveIsGenerated, this, onMoveIsDone, static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection));
    connect(player, Player::errorOccurred, this, [this](QString error) {
        qDebug() << error;
    }, static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection));
    connect(player, Player::nameReceived, this, [this](QString name) {
        qDebug() << name;
    }, static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection));
    connect(player, Player::versionReceived, this, [this](QString version) {
        qDebug() << version;
    }, static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection));
}

FUNC_RESULT Controller::undo() {
    FUNC_RESULT result;
    result = this->_position.undo();
    if (FUNC_RESULT_SUCCESS == result) {
        this->blackPlayer->goToMove(this->_position.moveHistorySize());
        this->whitePlayer->goToMove(this->_position.moveHistorySize());
        if (dynamic_cast<MousePlayer *>(this->currentPlayer()) != nullptr)
            this->currentPlayer()->genMove(this->currentPlayerSide());
    }
    return result;
}

FUNC_RESULT Controller::doMove(const Move &move) {
    side_T winner;
    FUNC_RESULT result;
    std::vector<Move> winMoves;

    if (this->_position.isGameOver(winner, winMoves))
        return FUNC_RESULT_FAILED;

    result = this->_position.doMove(move);
    if (FUNC_RESULT_SUCCESS != result)
        return result;

    this->currentPlayer()->doMove(move);

    if (!this->_position.isGameOver(winner, winMoves))
        this->currentPlayer()->genMove(this->currentPlayerSide());
    else
        emit this->gameIsOver(winner, winMoves);

    return FUNC_RESULT_SUCCESS;
}

FUNC_RESULT Controller::clear() {
    return this->_position.clear();
}

const Position *Controller::position() const {
    return &this->_position;
}

Player *Controller::currentPlayer() const {
    return this->currentPlayerSide() == SIDE_BLACK ? blackPlayer : whitePlayer;
}

FUNC_RESULT Controller::saveGame(const QString &filePath) {
    const Move *move;
    char color, row, column;
    QFile file;

    file.setFileName(filePath);
    if (!file.open(QFile::WriteOnly))
        return FUNC_RESULT_FAILED;

    file.write("(;GM[4]\n");
    file.write("FF[4]\n");
    file.write("CA[UTF-8]\n");
    file.write("AP[TicTacToeInterface]\n");
    file.write(QString().sprintf("SZ[%d]\n", std::max(this->position()->rowsCount(), this->position()->columnsCount())).toLatin1());
    file.write(QString().sprintf("PB[%s]\n", this->blackPlayer->name().toLatin1().data()).toLatin1());
    file.write(QString().sprintf("PW[%s]\n", this->whitePlayer->name().toLatin1().data()).toLatin1());

    for (coord_T index = 0; index < this->position()->moveHistorySize(); ++index) {
        move = this->position()->moveHistory() + index;
        color = index % 2 == 0 ? 'B' : 'W';
        row =  'a' + move->rowIndex;
        column = 'a' + move->columnIndex;
        file.write(QString().sprintf(";%c[%c%c]\n", color, column, row).toLatin1());
    }

    file.write(")");
    return FUNC_RESULT_SUCCESS;
}

Player *Controller::nextPlayer() const {
    return this->currentPlayerSide() == SIDE_BLACK ? whitePlayer : blackPlayer;
}

side_T Controller::currentPlayerSide() const {
    return this->_position.currentPlayer();
}

void Controller::onMoveIsDone(Move move) {
    FUNC_RESULT result = this->doMove(move);
    if (FUNC_RESULT_SUCCESS == result)
        emit this->moveIsDone(move);
    else
        emit this->moveIsIncorrect(move);
}

