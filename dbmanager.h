#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QSqlTableModel>

#include <memory>

class DbManager : QObject {
    Q_OBJECT
public:
    DbManager() = delete;
    static bool connectTo(const QString &dbFilePath);
    static QSqlTableModel *getEnginesModel(QObject *parent = nullptr);
    static QStringList getEngineNameList();
    static QStringList getEngineFilePathList();
    static QString getEngineOptionsByName(QString name);
private:
    static const constexpr char *DRIVER_TYPE = "QSQLITE",
            *CREATE_TABLE = "CREATE TABLE IF NOT EXISTS %s (%s);",
            *ENGINES_TABLE_NAME = "engines",
            *ENGINES_TABLE_COLUMNS = "engine_id INTEGER PRIMARY KEY AUTOINCREMENT, %s VARCHAR(1024) UNIQUE, %s VARCHAR(1024) UNIQUE, %s VARCHAR(1024)",
            *ENGINES_FILE_PATH_COLUMN = "file_path",
            *ENGINES_NAME_COLUMN = "name",
            *ENGINES_OPTIONS_COLUMN = "options";

    static QString getEnginesTableColumns();
    static QStringList getEngineList(const QString &columnName);
};

#endif // DBMANAGER_H
