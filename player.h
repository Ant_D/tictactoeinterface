#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>

#include "const.h"
#include "position.h"

class Player : public QObject {
    Q_OBJECT
public:
    explicit Player(const QString &nameReceived, QObject *parent = nullptr);
    virtual void startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength);
    virtual void genMove(side_T side) = 0;
    virtual void undoMove() = 0;
    virtual void doMove(const Move &move) = 0;
    virtual void clear() = 0;
    virtual void goToMove(coord_T moveNum) = 0;
    QString name() const;

protected:
    coord_T rowsCount, columnsCount, unitLength;
    QString _name;

signals:
    void moveIsGenerated(Move move);
    void errorOccurred(QString errorOccurred);
    void nameReceived(QString nameReceived);
    void versionReceived(QString versionReceived);
    void log(QString data);
};

#endif // PLAYER_H
