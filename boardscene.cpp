#include "boardscene.h"

#include <QDebug>
#include <QGraphicsRectItem>
#include <QMessageBox>
#include <QTransform>
#include <QVector>

#include <algorithm>

#include "position.h"
#include "stoneitem.h"

Q_DECLARE_METATYPE(Move)
Q_DECLARE_METATYPE(side_T)

BoardScene::BoardScene(QObject *parent) : QGraphicsScene(parent) {
    this->controller = new Controller(this);
    this->rowsCount = 0;
    this->columnsCount = 0;
    this->unitLength = 0;

    this->mousePlayer = new MousePlayer(this);

    qRegisterMetaType<Move>();
    qRegisterMetaType<side_T>();

    connect(this->controller, SIGNAL(moveIsDone(Move)), this, SLOT(onMoveIsDone(Move)), Qt::QueuedConnection);
    connect(this->controller, Controller::gameIsOver, this, [this](side_T side, std::vector<Move> winMoves) {
        this->isGameOver = true;
        this->winMoves = QVector<Move>::fromStdVector(winMoves);
        for (Move &move : this->winMoves)
            this->cell(move.rowIndex, move.columnIndex)->setWinStyle();
    }, Qt::QueuedConnection);

    this->installEventFilter(this);
}

void BoardScene::startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength, Player *blackPlayer, Player *whitePlayer) {
    FUNC_RESULT result;

    this->isGameOver = false;
    this->lastMoveCell = nullptr;
    this->winMoves.resize(0);

    blackPlayer = (nullptr == blackPlayer ? this->mousePlayer : blackPlayer);
    whitePlayer = (nullptr == whitePlayer ? this->mousePlayer : whitePlayer);

    this->connectPlayerError("Black player", blackPlayer);
    this->connectPlayerError("White player", whitePlayer);

    result = this->controller->startNewGame(rowsCount, columnsCount, unitLength, blackPlayer, whitePlayer);
    if (result != FUNC_RESULT_SUCCESS)
        return; // TODO: raise error

    this->destroyCells();

    this->rowsCount = rowsCount;
    this->columnsCount = columnsCount;
    this->unitLength = unitLength;

    this->createCells();
}

void BoardScene::setSceneRect(const QRectF &rect) {
    this->QGraphicsScene::setSceneRect(rect);
    this->updateCells();
}

void BoardScene::setSceneRect(qreal x, qreal y, qreal w, qreal h) {
    this->QGraphicsScene::setSceneRect(x, y, w, h);
    this->updateCells();
}

qreal BoardScene::cellSize() const {
    return std::min(this->sceneRect().width() / columnsCount, this->sceneRect().height() / rowsCount);
}

bool BoardScene::eventFilter(QObject *watched, QEvent *event) {
    static QVector<QEvent::Type> types = {
        QEvent::MouseButtonPress, QEvent::MouseButtonRelease, QEvent::MouseMove,
        QEvent::HoverEnter, QEvent::HoverLeave, QEvent::HoverMove,
        QEvent::GraphicsSceneMousePress, QEvent::GraphicsSceneMouseRelease, QEvent::GraphicsSceneMouseMove,
        QEvent::GraphicsSceneHoverEnter, QEvent::GraphicsSceneHoverLeave, QEvent::GraphicsSceneHoverMove,
    };

    if ((this->controller->currentPlayer() != this->mousePlayer || this->isGameOver) && types.contains(event->type()))
        return true;

    return QObject::eventFilter(watched, event);
}

side_T BoardScene::currentPlayerSide() const {
    return this->controller->currentPlayerSide();
}

void BoardScene::saveGame(const QString &filePath) const {
    this->controller->saveGame(filePath);
}

void BoardScene::createCells() {
    coord_T index;

    for (coord_T rowIndex = 0; rowIndex < this->rowsCount; ++rowIndex)
        for (coord_T columnIndex = 0; columnIndex < this->columnsCount; ++columnIndex) {
            index = this->cellIndex(rowIndex, columnIndex);
            this->cells[index] = new CellItem(rowIndex, columnIndex);
            this->addItem(this->cells[index]);
        }
    this->updateCells();
}

void BoardScene::updateCells() {
    QPointF center = this->sceneRect().center();
    qreal size = this->cellSize(),
            top = center.y() - size * this->rowsCount * 0.5,
            left = center.x() - size * this->columnsCount * 0.5;
    coord_T index;
    for (coord_T rowIndex = 0; rowIndex < this->rowsCount; ++rowIndex)
        for (coord_T columnIndex = 0; columnIndex < this->columnsCount; ++columnIndex) {
            index = this->cellIndex(rowIndex, columnIndex);
            this->cells[index]->updateRect(left + columnIndex * size, top + rowIndex * size, size, size);
        }
}

void BoardScene::destroyCells() {
    for (coord_T index = 0; index < this->rowsCount * this->columnsCount; ++index) {
        this->removeItem(this->cells[index]);
        delete this->cells[index];
    }
}

coord_T BoardScene::cellIndex(coord_T rowIndex, coord_T columnIndex) const {
    return rowIndex * this->columnsCount + columnIndex;
}

CellItem *BoardScene::cell(coord_T rowIndex, coord_T columnIndex) const {
    return this->cells[this->cellIndex(rowIndex, columnIndex)];
}

void BoardScene::connectPlayerError(const QString &color, Player *player) const {
    connect(player, Player::errorOccurred, this, [=](QString error) {
        QMessageBox(QMessageBox::Warning, "", color + "\n" + error).exec();
    }, static_cast<Qt::ConnectionType>(Qt::QueuedConnection | Qt::UniqueConnection));
}

void BoardScene::onMoveIsDone(Move move) {
    // TODO: check move for correctness
    CellItem *cell = this->cell(move.rowIndex, move.columnIndex);
    cell->placeStone(move.color);
    cell->placeMarker();
    if (nullptr != this->lastMoveCell)
        this->lastMoveCell->removeMarker();
    this->lastMoveCell = cell;
}

void BoardScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    CellItem *cellItem = nullptr;
    side_T player = this->controller->currentPlayerSide();

    for (QGraphicsItem *item = this->itemAt(mouseEvent->scenePos(), QTransform());
         nullptr != item && nullptr == cellItem;
         item = item->parentItem())
        cellItem = dynamic_cast<CellItem *>(item);

    if (nullptr != cellItem)
        emit mousePlayer->moveIsGenerated(Move(cellItem->rowIndex(), cellItem->columnIndex(), player));
}

void BoardScene::undo() {
    FUNC_RESULT result = this->controller->undo();
    if (FUNC_RESULT_SUCCESS == result) {
        const Move *move = this->controller->position()->lastMove();
        if (nullptr != this->lastMoveCell) {
            this->lastMoveCell->removeStone();
            this->lastMoveCell->removeMarker();
            this->lastMoveCell = nullptr;
        }
        if (nullptr != move) {
            this->lastMoveCell = this->cell(move->rowIndex, move->columnIndex);
            this->lastMoveCell->placeMarker();
        }
        for (Move &move: this->winMoves)
            this->cell(move.rowIndex, move.columnIndex)->setDefaultStyle();
        this->isGameOver = false;
    }

}
