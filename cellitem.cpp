#include "cellitem.h"

#include <QDebug>
#include <QPen>

#include "boardscene.h"

CellItem::CellItem(coord_T rowIndex, coord_T columnIndex, QGraphicsItem *parent) : QGraphicsRectItem(parent) {
    this->_rowIndex = rowIndex;
    this->_columnIndex = columnIndex;
    this->stoneItem = nullptr;
    this->markerItem = nullptr;
    this->_occupied = false;
    this->setDefaultStyle();
    this->setAcceptHoverEvents(true);
}

coord_T CellItem::rowIndex() const {
    return this->_rowIndex;
}

coord_T CellItem::columnIndex() const {
    return this->_columnIndex;
}

void CellItem::placeStone(side_T side, bool real) {
    if ((SIDE_BLACK == side || SIDE_WHITE == side) && !this->_occupied) {
        this->removeStone();
        this->stoneItem = new StoneItem(this->scaledRect(this->STONE_SIZE_FACTOR), side, !real, this);
        this->_occupied = real;
    }
}

void CellItem::removeStone() {
    if (nullptr == this->stoneItem)
        return;
    delete this->stoneItem;
    this->stoneItem = nullptr;
    this->_occupied = false;
}

void CellItem::placeMarker() {
    if (nullptr != this->markerItem)
        return;
    this->markerItem = new QGraphicsEllipseItem(this->scaledRect(this->MARKER_SIZE_FACTOR), this);
    if (nullptr == this->stoneItem || this->stoneItem->side() == SIDE_WHITE)
        this->markerItem->setPen(QPen(Qt::black));
    else
        this->markerItem->setPen(QPen(Qt::white));
}

void CellItem::removeMarker() {
    if (nullptr == this->markerItem)
        return;
    delete this->markerItem;
    this->markerItem = nullptr;
}

void CellItem::updateRect(qreal x, qreal y, qreal w, qreal h) {
    this->setRect(x, y, w, h);
    if (nullptr != this->stoneItem)
        this->stoneItem->setRect(this->scaledRect(this->STONE_SIZE_FACTOR));
    if (nullptr != this->markerItem)
        this->markerItem->setRect(this->scaledRect(this->MARKER_SIZE_FACTOR));
}

bool CellItem::occupied() const {
    return this->_occupied;
}

void CellItem::setDefaultStyle() {
    this->setBrush(QBrush(Qt::white));
}

void CellItem::setWinStyle() {
    this->setBrush(QBrush(Qt::green));
}

void CellItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    BoardScene *boardScene = dynamic_cast<BoardScene *>(this->scene());
    this->QGraphicsRectItem::hoverEnterEvent(event);
    if (!this->occupied() && nullptr != boardScene)
        this->placeStone(boardScene->currentPlayerSide(), false);
}

void CellItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    this->QGraphicsRectItem::hoverLeaveEvent(event);
    if (!this->occupied())
        this->removeStone();
}

qreal CellItem::size() const {
    return std::min(this->rect().width(), this->rect().height());
}

QRectF CellItem::scaledRect(qreal factor) {
    QRectF rect(this->rect());
    qreal newWidth = rect.width() * factor,
            newHeight = rect.height() * factor;
    QPointF center = rect.center();
    return QRectF(center.x() - 0.5 * newWidth,
                  center.y() - 0.5 * newHeight,
                  newWidth, newHeight);
}
