#ifndef STONE_ITEM_H
#define STONE_ITEM_H

#include <QGraphicsEllipseItem>

#include "const.h"

class StoneItem : public QGraphicsEllipseItem {
public:
    StoneItem(const QRectF &rect, side_T side, bool pale = false, QGraphicsItem *parent = nullptr);
    side_T side() const;

private:
    const qreal BORDER_WIDTH_FACTOR = 0.1;
    side_T _side;
};

#endif // STONE_ITEM_H
