#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>

#include "dbmanager.h"

const static char *DB_FILE_PATH = "db";

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;

    if (!DbManager::connectTo(DB_FILE_PATH)) {
        QMessageBox(QMessageBox::Critical, "Database error", "Unable to connect to database").exec();
        return -1;
    }

    w.showMaximized();

    return a.exec();
}
