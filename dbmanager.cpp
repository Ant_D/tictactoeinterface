#include "dbmanager.h"

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>

bool DbManager::connectTo(const QString &dbFilePath) {
    QSqlDatabase db = QSqlDatabase::addDatabase(DbManager::DRIVER_TYPE);
    db.setDatabaseName(dbFilePath);
    if (!db.open())
        return false;

    QString query = QString().sprintf(DbManager::CREATE_TABLE,
                                      DbManager::ENGINES_TABLE_NAME,
                                      DbManager::getEnginesTableColumns().toStdString().data());

    return QSqlQuery().exec(query);
}

QSqlTableModel *DbManager::getEnginesModel(QObject *parent) {
    QSqlTableModel *model = new QSqlTableModel(parent);
    model->setTable(DbManager::ENGINES_TABLE_NAME);
    return model;
}

QStringList DbManager::getEngineNameList() {
    return DbManager::getEngineList(DbManager::ENGINES_NAME_COLUMN);
}

QStringList DbManager::getEngineFilePathList() {
    return DbManager::getEngineList(DbManager::ENGINES_FILE_PATH_COLUMN);
}

QStringList DbManager::getEngineList(const QString &columnName) {
    auto engineModel = DbManager::getEnginesModel();
    QStringList engines;

    engineModel->select();

    for (int rowIndex = 0; rowIndex < engineModel->rowCount(); ++rowIndex)
        engines.append(engineModel->record(rowIndex).field(columnName).value().toString());

    return engines;
}

QString DbManager::getEngineOptionsByName(QString name) {
    auto engineModel = DbManager::getEnginesModel();
    QString options;

    engineModel->select();

    for (int rowIndex = 0; rowIndex < engineModel->rowCount(); ++rowIndex)
        if (engineModel->record(rowIndex).field(DbManager::ENGINES_NAME_COLUMN).value().toString() == name)
            options = engineModel->record(rowIndex).field(DbManager::ENGINES_OPTIONS_COLUMN).value().toString();

    return options;
}

QString DbManager::getEnginesTableColumns() {
    return QString().sprintf(DbManager::ENGINES_TABLE_COLUMNS, DbManager::ENGINES_NAME_COLUMN,
                             DbManager::ENGINES_FILE_PATH_COLUMN, DbManager::ENGINES_OPTIONS_COLUMN);
}
