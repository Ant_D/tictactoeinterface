#include "player.h"

Player::Player(const QString &name, QObject *parent) : QObject(parent) {
    this->_name = name;
}

void Player::startNewGame(coord_T rowsCount, coord_T columnsCount, coord_T unitLength) {
    this->rowsCount = rowsCount;
    this->columnsCount = columnsCount;
    this->unitLength = unitLength;
}

QString Player::name() const {
    return this->_name;
}
